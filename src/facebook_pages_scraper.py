import argparse
import math
import multiprocessing
import os
import random
import sys
import time
import warnings
import validators
import settings
from datetime import datetime, timedelta
from multiprocessing.context import Process
from threading import Thread

from scrapers.downloader import Downloader
from scrapers.middlewares import (
    DownloaderMiddleware, HttpScraperMiddleware, ScraperMiddleware)
from scrapers.pipelines import MongoDBPipeline
from utils.io import DataIO


class ScraperLoader:
    """
    A class responsible for loading and managing data scraping processes.

    Attributes:
        log_filepath (str): The path to the log file.
        output_filepath (str): The path to the output file.
    """

    def __init__(self, args):
        """
        Initialize the ScraperLoader object.

        Args:
            args: Command-line arguments containing log and output file paths.
        """
        self.log_filepath = str(args.log_filepath)
        self.output_filepath = str(args.output_filepath)

    def scrape_data_http(self, downloader, http_scraper_middleware, mongo_pipeline, session_input_args, category_object):
        """
        Execute a loop to scrape data using HTTP requests.

        Args:
            downloader: Downloader object for handling HTTP requests.
            http_scraper_middleware: Middleware for scraping data from HTTP responses.
            mongo_pipeline: Pipeline for storing data in MongoDB.
            session_input_args (dict): Session input arguments.
            category_object (dict): Category object containing scraping details.
        """
        # Initialize variables to keep track of the scraping process
        account_num = 0  # Current account index
        consec_blocked_times = 0  # Number of consecutive times blocked
        consec_ending_times = 0  # Number of consecutive times data ended
        is_blocked = False  # Flag indicating if scraping is blocked
        is_ending = False  # Flag indicating if data scraping is completed
        proxy_cluster = session_input_args.get('proxy_cluster', [])  # List of proxy configurations
        cookie_cluster = session_input_args.get('cookie_cluster', [])  # List of cookie configurations
        cpu_num = session_input_args.get('cpu_number', str())  # CPU number for logging
        thread_number = session_input_args.get('thread_number', str())  # Thread number for logging
        category_url = category_object.get('category_url', str())  # URL of the category to scrape
        missing_pages = category_object.get('missing_pages', [])  # List of missing page numbers to scrape
        latest_page = category_object.get('latest_page', 0)  # Latest scraped page number
        start_page = latest_page + 1  # Starting page for scraping
        account_batch_size = int(settings.ACCOUNT_BATCH_SIZE)  # Size of account batch

        # Check if the category URL is a valid URL
        if not validators.url(category_url):
            return

        # Check if both proxy and cookie clusters have the required batch size
        if not all(len(inp_cluster) == account_batch_size for inp_cluster in [proxy_cluster, cookie_cluster]):
            return

        # Enter the main scraping loop
        while True:
            http_request = None
            page_source = str()
            try:
                # Initialize an HTTP request object if not already created
                if http_request is None:
                    proxy_args = proxy_cluster[account_num % len(proxy_cluster)]
                    cookie_args = cookie_cluster[account_num % len(cookie_cluster)]
                    user_agent = DataIO().generate_user_agent()
                    http_request = downloader.initialize_request(cookie_args, proxy_args, user_agent)

                # Log information about the current scraping session
                print(f'{category_url}\nUsing CPU number: {cpu_num}, thread number: {thread_number}')

                # Scrape data from the category page and update variables accordingly
                _, latest_page, is_blocked, is_ending, page_source = http_scraper_middleware.scrape_page_by_category(
                    http_request, mongo_pipeline, category_url, start_page, missing_pages)
                start_page = latest_page

                # Check if scraping is blocked
                if is_blocked:
                    account_num += 1
                    consec_blocked_times += 1
                    # If blocked consecutively for the batch size, pause scraping for 5000 seconds
                    if consec_blocked_times >= settings.ACCOUNT_BATCH_SIZE:
                        print(f'Pausing: {category_url}')
                        time.sleep(5000)
                        consec_blocked_times = 0
                        random.shuffle(proxy_cluster)
                        random.shuffle(cookie_cluster)
                else:
                    # Reset consecutive blocking count if not blocked
                    consec_blocked_times = 0

                    # Change account after scraping a certain number of pages
                    if latest_page % settings.REQUEST_BATCH_SIZE == 0:
                        account_num += 1

                    # Check if data scraping for the category is completed
                    if is_ending:
                        account_num += 1
                        if consec_ending_times < 2:
                            consec_ending_times += 1
                        else:
                            return
                    else:
                        # Reset consecutive ending count if data is still available
                        consec_ending_times = 0
            except Exception as e:
                # Handle exceptions and move to the next account
                account_num += 1
                http_request = None
                current_class = self.__class__.__name__
                error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
                DataIO().write_to_log(error_message, self.log_filepath)

    def scrape_data_browser(self, downloader, downloader_middleware, scraper_middleware, mongo_pipeline, session_input_args, category_object):
        """
        Execute a loop to scrape data using a web browser.

        Args:
            downloader: Downloader object for handling web browser instances.
            downloader_middleware: Middleware for managing web browser instances.
            scraper_middleware: Middleware for scraping data from web pages.
            mongo_pipeline: Pipeline for storing data in MongoDB.
            session_input_args (dict): Session input arguments.
            category_object (dict): Category object containing scraping details.
        """
        # Initialize variables to keep track of the scraping process
        account_num = 0  # Current account index
        consec_blocked_times = 0  # Number of consecutive times blocked
        consec_ending_times = 0  # Number of consecutive times data ended
        is_blocked = False  # Flag indicating if scraping is blocked
        is_ending = False  # Flag indicating if data scraping is completed
        proxy_cluster = session_input_args.get('proxy_cluster', [])  # List of proxy configurations
        cookie_cluster = session_input_args.get('cookie_cluster', [])  # List of cookie configurations
        cpu_num = session_input_args.get('cpu_number', str())  # CPU number for logging
        thread_number = session_input_args.get('thread_number', str())  # Thread number for logging
        category_url = category_object.get('category_url', str())  # URL of the category to scrape
        missing_pages = category_object.get('missing_pages', [])  # List of missing page numbers to scrape
        latest_page = category_object.get('latest_page', 0)  # Latest scraped page number
        start_page = latest_page + 1  # Starting page for scraping
        account_batch_size = int(settings.ACCOUNT_BATCH_SIZE)  # Size of account batch

        # Check if the category URL is a valid URL
        if not validators.url(category_url):
            return

        # Check if both proxy and cookie clusters have the required batch size
        if not all(len(inp_cluster) == account_batch_size for inp_cluster in [proxy_cluster, cookie_cluster]):
            return

        # Enter the main scraping loop
        while True:
            try:
                # Initialize a web browser instance if not already created
                user_agent = DataIO().generate_user_agent()
                proxy_args = proxy_cluster[account_num % len(proxy_cluster)]
                cookie_args = cookie_cluster[account_num % len(cookie_cluster)]
                chrome_driver = downloader.initialize_chrome_driver(proxy_args, user_agent)
                chrome_driver = downloader_middleware.add_facebook_cookies(chrome_driver, cookie_args)

                # Fetch the web page using the web browser instance
                chrome_driver = downloader.fetch_web_page(chrome_driver, category_url)
                print(f'{category_url}\nUsing CPU number: {cpu_num}, thread number: {thread_number}')

                # Scrape data from the category page and update variables accordingly
                _, latest_page, is_blocked, is_ending = scraper_middleware.scrape_page_by_category(
                    chrome_driver, mongo_pipeline, category_url, start_page, missing_pages)
                start_page = latest_page

                # Check if scraping is blocked
                if is_blocked:
                    consec_blocked_times += 1
                    # If blocked consecutively for the batch size, pause scraping for 5000 seconds
                    if consec_blocked_times >= settings.ACCOUNT_BATCH_SIZE:
                        print(f'Pausing: {category_url}')
                        time.sleep(5000)
                        consec_blocked_times = 0
                        random.shuffle(proxy_cluster)
                        random.shuffle(cookie_cluster)
                else:
                    # Reset consecutive blocking count if not blocked
                    consec_blocked_times = 0

                # Change account after scraping a certain number of pages
                if latest_page % settings.REQUEST_BATCH_SIZE == 0:
                    account_num += 1

                # Check if data scraping for the category is completed
                if is_ending:
                    account_num += 1
                    # Log a screenshot of the browser for investigation
                    DataIO().capture_screenshot(chrome_driver, settings.OUTPUT_SCREENSHOT_DIR)
                    downloader.close_chrome_driver(chrome_driver)
                    if consec_ending_times < 2:
                        consec_ending_times += 1
                    else:
                        return
                else:
                    # Reset consecutive ending count if data is still available
                    consec_ending_times = 0
            except Exception as e:
                # Handle exceptions and move to the next account
                account_num += 1
                downloader.close_chrome_driver(chrome_driver)
                current_class = self.__class__.__name__
                error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
                DataIO().write_to_log(error_message, self.log_filepath)

    @classmethod
    def scrape_multiple_categories(cls, session_input_args):
        """
        Execute a loop to scrape data for multiple categories.

        Args:
            session_input_args (dict): Session input arguments.
        """
        try:
            # Initialize various components for scraping
            downloader = Downloader(cls)  # Create a Downloader instance
            downloader_middleware = DownloaderMiddleware(cls)  # Create a DownloaderMiddleware instance
            scraper_middleware = ScraperMiddleware(cls)  # Create a ScraperMiddleware instance
            http_scraper_middleware = HttpScraperMiddleware(cls)  # Create an HttpScraperMiddleware instance
            mongo_pipeline = MongoDBPipeline(cls)  # Create a MongoDBPipeline instance

            # Iterate over a cluster of category objects and scrape data
            category_object_cluster = session_input_args.get('category_cluster', [])
            for category_object in category_object_cluster:
                if settings.USE_SELENIUM:
                    # Scrape data using Selenium for this category
                    cls.scrape_data_browser(
                        cls, downloader, downloader_middleware,
                        scraper_middleware, mongo_pipeline, session_input_args, category_object)
                else:
                    # Scrape data using HTTP for this category
                    cls.scrape_data_http(
                        cls, downloader, http_scraper_middleware,
                        mongo_pipeline, session_input_args, category_object)
        except Exception as e:
            current_class = cls.__name__
            # Handle exceptions and log errors
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, cls.log_filepath)

    @classmethod
    def count_scraping_speed(cls):
        """
        Execute a loop to count scraping speed.

        This method calculates the scraping speed and logs it at regular intervals.
        """
        try:
            mongo_pipeline = MongoDBPipeline(cls)  # Create a MongoDBPipeline instance
            while True:
                try:
                    current_time = DataIO().get_current_time()  # Get the current time

                    # Calculate the seconds since the last minute
                    seconds_since_last_minute = current_time.second

                    # Calculate the minutes since the last hour
                    minutes_since_last_hour = (current_time.minute +
                                            current_time.second / 60.0)

                    # Calculate the hours since the last day
                    hours_since_last_day = (current_time.hour +
                                            minutes_since_last_hour / 60.0)

                    # Calculate and log scraping speed for the last minute
                    if seconds_since_last_minute == 0:
                        from_date = current_time - timedelta(minutes=1)
                        mongo_pipeline.calculate_and_log_scraping_speed(from_date, current_time)

                    # Calculate and log scraping speed for the last hour
                    if minutes_since_last_hour == 0:
                        from_date = current_time - timedelta(hours=1)
                        mongo_pipeline.calculate_and_log_scraping_speed(from_date, current_time)

                    # Calculate and log scraping speed for the last day
                    if hours_since_last_day == 0:
                        from_date = current_time - timedelta(days=1)
                        mongo_pipeline.calculate_and_log_scraping_speed(from_date, current_time)

                except Exception as e:
                    current_class = cls.__name__
                    error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
                    DataIO().write_to_log(error_message, cls.log_filepath)

                time.sleep(settings.REPEAT_INTERVAL)  # Sleep for the specified repeat interval

        except Exception as e:
            current_class = cls.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, cls.log_filepath)


class ParallelFacebookScraper:
    """
    A class responsible for managing parallel web scraping tasks.

    Attributes:
        args (dict): Command-line arguments.
        log_filepath (str): The path to the log file.
        proxies (list): List of proxy settings.
        cookies (list): List of cookie settings.
        user_agents (list): List of user-agent settings.
        categories (list): List of category objects for scraping.
    """

    def __init__(self, args):
        # Initialize object with provided arguments
        self.args = args
        self.log_filepath = self.args.log_filepath

        # Load proxy, cookie, and user-agent settings from files
        self.proxies = DataIO().load_proxies(settings.PROXY_FILE_DIR, ratio=1)
        self.cookies = DataIO().load_cookies(settings.COOKIE_FILE_DIR)
        self.user_agents = DataIO().load_user_agents(settings.USER_AGENT_FILE_DIR)

        # Load and process the latest state of web scraping tasks
        self.categories = self.load_latest_scraping_state(settings.INPUT_CATEGORY_DIR, settings.OUTPUT_LOG_DIR)

    @classmethod
    def load_latest_scraping_state(cls, input_category_directory):
        """
        Load and process the latest state of web scraping tasks.

        Args:
            input_category_directory (str): The directory containing input category files.

        Returns:
            list: List of category objects with scraping information.
        """
        try:
            # Initialize an empty list to store the latest state of scraping tasks
            latest_scraping_state = list()

            # Create a MongoDBPipeline instance for querying data from MongoDB
            mongo_pipeline = MongoDBPipeline(cls)

            # Get a list of subdirectories in the input category directory with '.txt' extension
            category_directories = DataIO().get_subdirectories(input_category_directory, ['.txt'])

            # Read category information from the input category files
            input_category_info = DataIO().read_multi_dimensional_data(category_directories, num_columns=1)

            # Create a set of input category URLs, replacing 'https://www.facebook.com' with 'https://m.facebook.com'
            input_category_set = set(map(lambda entry: entry[0].replace('https://www.facebook.com', 'https://m.facebook.com'), input_category_info))

            # Query MongoDB for existing category URLs
            output_category_set = mongo_pipeline.process_dataset(query_field_name='category_url')

            # Query MongoDB for existing paging URLs
            output_paging_set = mongo_pipeline.process_dataset(query_field_name='paging_url')

            # Query MongoDB for existing Facebook page URLs
            output_facebook_page_set = mongo_pipeline.process_dataset(query_field_name='facebook_page_url')

            # Calculate missing categories by finding the difference between input and output category sets
            missing_categories = input_category_set - output_category_set

            # Iterate through each output category URL
            for output_category in output_category_set:
                if output_category in input_category_set:
                    # Filter output paging URLs belonging to the current category
                    output_category_pages = set(filter(lambda entry: entry.startswith(output_category), output_paging_set))

                    # Extract page parameters and convert them to integers
                    output_page_params = list(set(map(lambda entry: entry.replace(output_category, '').replace('?page=', ''), output_category_pages)))
                    output_page_params = list(filter(lambda entry: entry != '', output_page_params))
                    output_page_params = set(map(lambda entry: int(entry), output_page_params))

                    # Find the latest page number
                    latest_page = max(output_page_params)

                    # Create a set of assumed pages from 1 to the latest page
                    assumed_pages = set(range(1, latest_page + 1))

                    # Calculate missing pages by finding the difference between assumed and actual pages
                    missing_pages = output_page_params.union(assumed_pages) - output_page_params.intersection(assumed_pages)

                    # Append category information to the latest state list
                    latest_scraping_state.append({
                        'category_url': output_category,
                        'latest_page': latest_page,
                        'missing_pages': list(missing_pages)
                    })

            # Print the total number of Facebook pages
            print(f'Total Facebook Pages: {len(output_facebook_page_set)}')

            # Iterate through missing categories and add them to the latest state list with default values
            for missing_category in missing_categories:
                latest_scraping_state.append({
                    'category_url': missing_category,
                    'latest_page': 0,
                    'missing_pages': list()
                })

            # Sort the latest state list based on category URLs
            latest_scraping_state = sorted(latest_scraping_state, key=lambda entry: entry['category_url'])

            # Print the latest state
            print(*latest_scraping_state, sep='\n')

        except Exception as e:
            # Handle any exceptions that may occur and log the error
            latest_scraping_state = list()
            current_class = cls.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, cls.log_filepath)

        finally:
            # Return the latest state list (empty if an error has occurred)
            return latest_scraping_state

    def distribute_scraping_input(self, batch_num, account_batch_size, category_batch_size):
        """
        Distribute input data for each scraping task.

        Args:
            batch_num (int): Batch number.
            account_batch_size (int): Size of the account batch.
            category_batch_size (int): Size of the category batch.

        Returns:
            dict: Session input arguments.
        """
        return {
            'cookie_cluster': self.cookies[batch_num * account_batch_size: (batch_num + 1) * account_batch_size],
            'proxy_cluster': self.proxies[batch_num * account_batch_size: (batch_num + 1) * account_batch_size],
            'user_agent_cluster': self.user_agents[batch_num * account_batch_size: (batch_num + 1) * account_batch_size],
            'category_cluster': self.categories[batch_num * category_batch_size: (batch_num + 1) * category_batch_size]
        }

    def initialize_web_scraping_job(self, command_line_args, session_input_args):
        """
        Initialize web scraping tasks.

        Args:
            command_line_args (dict): Command-line arguments.
            session_input_args (dict): Session input arguments.
        """
        # Create a scraper object and start category loop
        web_scraper = ScraperLoader(command_line_args)
        web_scraper.scrape_multiple_categories(session_input_args)

    def initialize_speed_analyzer(self, command_line_args):
        """
        Initialize web scraping speed analyzer.

        Args:
            command_line_args (dict): Command-line arguments.
        """
        # Create a scraper object and start speed analysis loop
        web_scraper = ScraperLoader(command_line_args)
        web_scraper.count_scraping_speed()

    def schedule_web_scraping_tasks(self, cpu_number, total_cpus):
        """
        Schedule web scraping tasks.

        Args:
            cpu_number (int): The current CPU number.
            total_cpus (int): Total number of CPUs.
        """
        try:
            # Create a list to store thread objects
            thread_list = list()

            # Get settings values for account batch size, number of threads, and categories per VPS
            account_batch_size = settings.ACCOUNT_BATCH_SIZE
            threads_per_process = settings.NUMBER_THREAD
            categories_per_vps = math.ceil(settings.NUMBER_CATEGORIES_PER_VPS / (threads_per_process * total_cpus))

            # Loop through a range of thread numbers specific to the current CPU
            for thread_number in range(cpu_number * threads_per_process, (cpu_number + 1) * threads_per_process):
                # Distribute input arguments for the current thread
                thread_args = self.distribute_scraping_input(thread_number, account_batch_size, categories_per_vps)

                # Add CPU number and thread number information to the input arguments
                thread_args.update({
                    'cpu_number': cpu_number + 1,
                    'thread_number': thread_number + 1
                })

                # Create a new thread and initialize a web scraping job with the provided arguments
                thread = Thread(target=self.initialize_web_scraping_job, args=[self.args, thread_args])
                thread.daemon = True  # Set the thread as a daemon so it doesn't block program exit
                thread.start()  # Start the thread
                thread_list.append(thread)  # Add the thread to the list

            # Wait for all threads to complete
            for _ in range(len(thread_list)):
                thread = thread_list[_]
                thread.join()

        except Exception as e:
            # Handle any exceptions that may occur and log the error
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.args.log_filepath)

    def schedule_planning(self):
        """
        Schedule web scraping tasks and analyzer.
        """
        try:
            # Create an empty list to store process objects
            processes = list()

            # Get the number of Virtual Private Servers (VPS) and the number of CPUs to use
            vps_count = int(self.args.vps_count)
            cpu_count = multiprocessing.cpu_count() if settings.USE_DEFAULT_CORE_NUMBER else settings.NUMBER_CORE

            # Create a separate process for the web scraping analyzer
            analysis_process = Process(target=self.initialize_speed_analyzer, args=[self.args])
            analysis_process.daemon = True  # Set the process as a daemon so it doesn't block program exit
            analysis_process.start()  # Start the analyzer process
            processes.append(analysis_process)  # Add the analyzer process to the list

            # Loop through a range of CPU numbers for the current VPS
            for cpu_num in range((vps_count - 1) * cpu_count, vps_count * cpu_count):
                # Create a separate process for scheduling web scraping tasks
                scraper_process = Process(target=self.schedule_web_scraping_tasks, args=[cpu_num, cpu_count])
                scraper_process.daemon = True  # Set the process as a daemon so it doesn't block program exit
                scraper_process.start()  # Start the web scraping process
                processes.append(scraper_process)  # Add the web scraping process to the list

            # Wait for all processes to complete
            for _ in range(len(processes)):
                scraper_process = processes[_]
                scraper_process.join()

        except Exception as e:
            # Handle any exceptions that may occur and log the error
            current_class = self.__class__.__name__
            error_message = f'({current_class} class) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'
            DataIO().write_to_log(error_message, self.args.log_filepath)


# Check if the script is being run as the main program
if __name__ == '__main__':
    warnings.filterwarnings('ignore')

    # Parse command-line arguments using the argparse library
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--vps_count', default='1')
    args = arg_parser.parse_args()

    # Generate log file and output file paths based on the current time
    current_time = DataIO().get_current_time()
    log_filepath = settings.OUTPUT_LOG_DIR + \
        current_time.strftime(
            '%Y-%m-%d %H-%M-%S-%f') + settings.LOG_EXT
    output_filepath = settings.OUTPUT_LOG_DIR + \
        current_time.strftime(
            '%Y-%m-%d %H-%M-%S-%f') + settings.OUTPUT_EXT

    try:
        # Set log and output file paths as attributes of args
        setattr(args, 'log_filepath', log_filepath)
        setattr(args, 'output_filepath', output_filepath)

        # Initialize an instance of the ParallelFacebookScraper class with args
        pfs = ParallelFacebookScraper(args)

        # Schedule web scraping tasks and analyzer
        pfs.schedule_planning()
    except Exception as e:
        # Get the name of the current script file
        current_file_name = os.path.basename(sys.argv[0])

        # Create a log content string with error details
        error_message = f'({current_file_name} file) | An error has occurred at line {str(sys.exc_info()[-1].tb_lineno)}: {str(e)}'

        # Write the error log to the specified log file (log_filepath)
        DataIO().write_to_log(error_message, log_filepath)
