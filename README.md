# Facebook Pages Scraping Tool

## Description

The Facebook Pages Scraping Tool is a Python-based data collection tool designed to gather information from Facebook pages. The tool aims to efficiently scrape data from a massive number of Facebook Pages, including basic details like URL and Page name.

### Screenshot Demonstration

<figure>
    <center>
        <img src="src/data/output/screenshots/Screenshot 2020-12-25 05-10-25-531489.png">
        <figcaption style="text-align:center"><i>This screenshot logs Facebook Pages from a page within https://www.facebook.com/pages/category/ during data scraping</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="src/data/output/screenshots/Screenshot 2020-12-26 19-54-49-181065.png">
        <figcaption style="text-align:center"><i>This screenshot logs Facebook Pages from a page within https://www.facebook.com/pages/category/ during data scraping</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="src/data/output/screenshots/Screenshot 2020-12-26 21-02-15-101918.png">
        <figcaption style="text-align:center"><i>This screenshot logs the process using cookies provided by automatic account creation tools to log in to Facebook and scrape data</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="src/data/output/screenshots/Screenshot 2020-12-26 20-46-14-237696.png">
        <figcaption style="text-align:center"><i>This screenshot logs the occurrence when multiple abnormal data scraping requests were detected and blocked by the Facebook system</i></figcaption>
    </center>
</figure>
<figure>
    <center>
        <img src="src/data/output/screenshots/Screenshot 2020-12-26 20-50-20-933053.png">
        <figcaption style="text-align:center"><i>This screenshot logs the process using cookies provided by automatic account creation tools to log in to Facebook but failed due to detection and account suspension by the Facebook system</i></figcaption>
    </center>
</figure>

### Video Demonstration

[![](https://img.youtube.com/vi/0dFgvRPxTUU/0.jpg)](https://www.youtube.com/watch?v=0dFgvRPxTUU)

[![](https://img.youtube.com/vi/E5OUGJgZ2O8/0.jpg)](https://www.youtube.com/watch?v=E5OUGJgZ2O8)

### Project Directory Structure

The directory structure for the Facebook Pages Scraping Tool project is organized as follows:

```
project-root/
│   .gitignore
│   CODE_OF_CONDUCT.md
│   CONTRIBUTING.md
│   LICENSE
│   README.md
│   requirements.txt
│   runtime.txt
│   
\---src
    |   facebook_pages_scraper.py
    |   settings.py
    |   __init__.py
    |   
    +---data
    |   +---input
    |   |   +---categories
    |   |   |       category_urls.txt
    |   |   |       
    |   |   \---user_agents
    |   |           use_agents.txt
    |   |           
    |   \---output
    |       +---analysis
    |       |       minutely_report.txt
    |       |       
    |       +---logs
    |       |       2020-12-09 02-02-34-110082.csv
    |       |       2020-12-25 01-07-00-534815.txt
    |       |       
    |       +---mongodb
    |       |       facebook_page.json
    |       |       
    |       \---screenshots
    |               Screenshot 2020-12-25 01-54-22-303870.png
    +---libs
    |       chromedriver.exe
    |       
    +---scrapers
    |       downloader.py
    |       middlewares.py
    |       pipelines.py
    |       __init__.py
    |       
    \---utils
            io.py
            __init__.py
```

This directory structure represents the layout of Facebook Pages Scraping Tool project, including essential files and folders such as Python scripts, settings, libraries, scrapers, and utilities. It aids in maintaining code organization and separation of responsibilities.

### My Approach to Scraping Facebook Pages Data

When scraping Facebook Pages data from Facebook, I've implemented several strategies and solutions to ensure smooth and efficient data retrieval, while also minimizing the risk of being blocked or identified as a scraper. Here's how I've approached this:

- Distributed Scraping with VPS:
    I divided my scraping work among many machines and IP addresses. I used Virtual Private Servers (VPS) to handle this work distribution. Each VPS acts like a control center for multiple Python processes, allowing for faster and more efficient scraping. This method makes sure I use our resources to the fullest and get the data much quicker. It doesn't just make things work better but also eases the pressure on one IP address. This means it's less likely to raise concerns from VPS providers.

- Multi-Processing for Efficient Resource Usage:
    Within each VPS, I've adopted a multi-processing strategy to make the most of the available CPU cores. Each process operates independently, handling a specific portion of the scraping tasks. This concurrent processing ensures optimal utilization of system resources and contributes to the overall scraping speed.

- Multi-Threading for I/O Optimization:
    Further optimizing the scraping process, I've employed multi-threading within each Python process to address I/O constraints effectively. By dedicating threads to I/O-bound tasks such as sending requests and handling responses, I've reduced idle time and improved the overall scraping efficiency.

- Use of Proxies:
    I've recognized the importance of using proxies in my scraping process. Proxies have played a crucial role in distributing my requests across different IP addresses, reducing the chances of triggering rate limits or getting flagged as a scraper. Specifically, I've opted for residential proxies over datacenter proxies. Residential proxies provide me with IP addresses associated with real users, which significantly reduces the likelihood of being blocked.

- Rate Limiting:
    To emulate natural user behavior and avoid detection, I've made sure not to overwhelm Facebook with too many requests in a short time frame. By implementing rate limiting, I've effectively controlled the frequency of my requests. This approach helps me mimic the behavior of typical users and reduces the risk of being identified as a bot.

- User-Agent Rotation:
    To further avoid detection, I've varied the User-Agent header in my HTTP requests. This simple yet effective technique makes it appear as if my requests are originating from different browsers and devices, preventing detection based on consistent User-Agent values.

- Session Management:
    I've recognized the importance of managing sessions effectively. Maintaining separate sessions for each request and avoiding the use of the same session for all requests has helped me bypass some of Facebook's bot detection mechanisms. This approach ensures that my interactions with Facebook appear more like those of a human user.

- Emulation of Human Behavior:
    Mimicking human behavior has been a key aspect of my scraping strategy. To achieve this, I've incorporated various tactics such as randomizing delays between requests, scrolling through pages, clicking on random elements, and simulating interactions using different browsers and devices. These efforts collectively make my scraping activities closely resemble those of genuine users.

- Cookie Handling:
    When it comes to cookies, I've exercised caution, especially when dealing with cookies generated from automatic account creation tools. I'm aware that Facebook may have mechanisms to detect such cookies. To mitigate this risk, I've considered manual cookie creation or obtaining cookies from legitimate accounts.

- Monitoring for Changes:
    I've remained vigilant regarding potential changes to Facebook's website structure and anti-scraping mechanisms. Continuous monitoring has allowed me to adapt my scraping code promptly in response to any alterations. This adaptability ensures that my scraping activities remain effective and undetected.

- IP Rotation:
    Regularly rotating my proxies and IP addresses has been a critical part of my strategy to avoid being blocked. Utilizing IP rotation services has automated this process, reducing the risk of detection.

- Robust Error Handling:
    I've implemented robust error handling mechanisms in my code. These mechanisms enable me to gracefully handle unexpected situations and prevent sudden spikes in requests. This approach ensures the stability of my scraping process.

By incorporating these strategies and solutions into my scraping approach, I've aimed to strike a balance between data retrieval efficiency and compliance with Facebook's policies. It's crucial to conduct web scraping within legal and ethical boundaries while respecting the terms of service of the websites being scraped. You can view a sample dataset containing over 3 million Facebook Page URLs that I scraped from Facebook, provided in [facebook_page.json](/src/data/output/mongodb/facebook_page.json) file, in comparison to the expected 300 million Facebook Page URLs from my employer.

# Data Flow

![Scrapy Architecture](https://docs.scrapy.org/en/latest/_images/scrapy_architecture_02.png)

The following diagram illustrates the data flow organization structure of the Facebook Pages Scraping Tool project. This structure draws inspiration from Scrapy's architecture, enhancing organization and modularity for efficient web scraping. The key components of the project are described below, with corresponding Python classes and file references.

- **Scrapy Engine**

    The Scrapy Engine serves as the core of the Facebook Pages Scraping Tool. It controls the data flow, manages parallel web scraping tasks, enhances efficiency, and manages various components. The main classes responsible for this component are:

    - `ParallelFacebookScraper` in the file `src\facebook_pages_scraper.py` (A class responsible for managing parallel web scraping tasks).
    - `ScraperLoader` in the file `src\facebook_pages_scraper.py` (A class responsible for loading and managing data scraping processes).

- **Downloader**

    The Downloader component is responsible for fetching web pages based on the requests generated by the engine. It retrieves web content and sends it back to the engine for further processing. The class handling this component is:

    - `Downloader` in the file `src\scrapers\downloader.py` (A class responsible for downloading tasks).

- **Spider Middlewares**

    Spider Middlewares are hooks that process requests and responses between the engine and the spiders. They parse responses, extract items, and generate additional requests to follow links or access more pages on the site. The middleware classes include:

    - `DownloaderMiddleware` in the file `src\scrapers\middlewares.py` (Middleware for downloading content).
    - `ScraperMiddleware` in the file `src\scrapers\middlewares.py` (Middleware for web scraping).
    - `HttpScraperMiddleware` in the file `src\scrapers\middlewares.py` (Middleware for HTTP-based web scraping).

- **MongoDB Integration**

    MongoDB integration is achieved through specific pipeline components and threads for efficient data handling. These components include:

    - `MongoInsertThread` in the file `src\scrapers\pipelines.py` (A thread for inserting data into MongoDB).
    - `MongoQuerysetThread` in the file `src\scrapers\pipelines.py` (A thread for querying data from MongoDB).
    - `SpeedAnalysisThread` in the file `src\scrapers\pipelines.py` (A thread for analyzing the speed of data retrieval).
    - `MongoDBPipeline` in the file `src\scrapers\pipelines.py` (A MongoDB pipeline for handling data processing).

- **Utility Classes**

    Utility classes are employed for data input and output operations, as well as general data handling. These include:

    - `DataIO` in the file `src\utils\io.py` (Utility class for handling data input and output operations).

This well-structured project architecture enhances code organization and maintainability, making it easier to manage and extend the Facebook Pages Scraping Tool's functionality.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [Credits](#credits)
- [License](#license)

## Installation

To set up the tool, follow these steps:

### Preinstalled

#### Create Requirements

```
$ sudo pipreqs --encoding=utf8 --force
```

#### Installation of Desktop (GUI) on Ubuntu Server 18.04 LTS

```
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install tasksel
$ sudo tasksel
```

#### Creating SWAP for Ubuntu 18.04 VPS

```
sudo dd if=/dev/zero of=/swapfile bs=1024 count=8192k
mkswap /swapfile
swapon /swapfile
echo /swapfile none swap defaults 0 0 >> /etc/fstab
chown root:root /swapfile
chmod 0600 /swapfile
sudo reboot
```

#### Environment Setup on Ubuntu 18.04 LTS

```
$ sudo apt upgrade
$ sudo apt-get update

$ sudo wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
$ sudo apt-get -f install
$ sudo dpkg --force-depends -i teamviewer_amd64.deb
$ sudo apt install ./teamviewer_amd64.deb

$ sudo apt install python3.8
$ sudo apt update
$ sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
$ sudo apt-get -y install python3-pip

$ sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
$ sudo dpkg -i google-chrome-stable_current_amd64.deb

$ sudo pip3 install selenium
$ sudo apt-get install chromium-chromedriver
$ sudo which chromedriver
$ sudo apt install wine-stable
$ sudo chmod -R 777 facebook-page-scraper-tool/
$ sudo pip3 install -r requirements.txt
```

#### MongoDB Installation

```
$ sudo wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
$ sudo apt-get install gnupg
$ sudo wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
$ echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
$ sudo apt-get update
$ sudo apt-get install dselect
$ sudo dselect update
$ echo "mongodb-org hold" | sudo dpkg --set-selections
$ echo "mongodb-org-server hold" | sudo dpkg --set-selections
$ echo "mongodb-org-shell hold" | sudo dpkg --set-selections
$ echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
$ echo "mongodb-org-tools hold" | sudo dpkg --set-selections
$ sudo apt-get install mongodb-org-tools
$ sudo apt-get install -y mongodb-org
$ sudo systemctl start mongod
$ sudo vim /etc/mongod.conf
```

Add the following lines:

```
security:
    authorization: 'enabled'
```
```
$ sudo service mongod restart
$ sudo mongo
> use admin;
> db.createUser({
      user: "<username>",
      pwd: "<password>",
      roles: [
                { role: "userAdminAnyDatabase", db: "<username>" },
                { role: "readWriteAnyDatabase", db: "<username>" },
                { role: "dbAdminAnyDatabase",   db: "<username>" }
             ]
  });
```
```
$ sudo service mongod restart
```

Connection:

```
mongodb://<username>:<password>@<host>:<port>
```

Use the MongoDB Shell to create an index for MongoDB:

```
> show dbs
> use Log
> db.facebook_page.createIndex( { "facebook_page_url": 1 }, { unique: true, dropDups: true })

{
  createdCollectionAutomatically: true,
  numIndexesBefore: 1,
  numIndexesAfter: 2,
  ok: 1
}
```

### Running the Tool

1. Create a virtual environment for your specific environment, whether it's Windows or Ubuntu.

    For Windows:

    ```
    $ python -m venv venv
    ```

    For Ubuntu:

    ```
    $ python3 -m venv venv
    ```

2. Activate the virtual environment:

    For Windows:

    ```
    $ venv\Scripts\activate
    ```

    For Ubuntu:

    ```
    $ source venv/bin/activate
    ```

3. Install the required modules using the following command:

    ```
    $ pip3 install -r requirements.txt
    ```

4. Run the tool with the following command:

    For Windows:

    ```
    $ py -3 facebook_pages_scraper.py
    ```

    For Ubuntu:

    ```
    $ python3 facebook_pages_scraper.py
    ```

## Usage

The employer's task was to develop a tool using the Python programming language to collect data from approximately 300 million Facebook Pages, gathering basic information such as URL and Page name. The tool is designed to perform the following actions:

- Retrieve all category lists from the URL https://www.facebook.com/pages/category/ along with the corresponding category URLs and save them to a CSV file.
- Proceed to collect data for all Facebook Pages displayed under each category from the category link and save this information to a CSV file.
- Utilize a combination of multiple proxies, multi-threading, and multi-processing in Python to accelerate the data collection process and optimize resource usage.
- Employ automated tools to create multiple Facebook accounts on iOS devices and obtain cookies for these accounts.
- Utilize cookies to enable automatic simultaneous logins on different threads. Each thread uses Selenium to create a browser window and handle information to bypass Facebook's Captcha verification.
- Send requests to pagination links on different browser windows for each main category to collect data.
- Use VPN in conjunction with proxies to prevent VPS providers from automatically restarting VPS due to excessive requests sent from a single VPS.
- Implement MongoDB to store data when large-scale information collection is required.
- Incorporate a feature to measure the speed of Facebook Page data collection by the hour and by the day.

## Contributing

If you want to contribute to this project, please follow the guidelines outlined in the [CONTRIBUTING.md](CONTRIBUTING.md) file.

## Credits

- Author: Nguyen Truong Long
- Reference Source: [Scrapy Documentation](https://docs.scrapy.org/en/latest/topics/architecture.html)

## License

This project is licensed under a closed-source license agreement. See the [LICENSE](LICENSE) file for more details. For inquiries, contact Nguyen Truong Long.
